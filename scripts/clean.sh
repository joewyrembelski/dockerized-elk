docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker container prune -f
# docker rmi $(docker images -a -q)
docker volume prune -f
# docker images prune -f
docker system prune -f

rm config/elasticsearch/elasticsearch.crt
rm config/elasticsearch/elasticsearch.key
rm config/elasticsearch/elasticsearch.keystore
rm config/kibana/kibana.crt
rm config/kibana/kibana.key
rm config/kibana/kibana.keystore
rm config/logstash/logstash.crt
rm config/logstash/logstash.key
rm config/logstash/logstash.keystore
rm -rf config/ssl/ca/
rm config/ssl/docker-cluster-ca.zip
rm config/ssl/docker-cluster.zip
rm config/apm-server/apm-server.keystore
rm config/filebeat/filebeat.keystore
rm config/heartbeat/heartbeat.keystore
rm config/metricbeat/metricbeat.keystore
rm config/packetbeat/packetbeat.keystore

rm /tmp/logstash/*
